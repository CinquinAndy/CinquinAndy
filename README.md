# Hi ! I'm <a href="https://andy-cinquin.fr/">Andy</a> ! 


Enthusiastic, curious, patient and understanding French Freelance Developer.  
I have a keen interest in a lot of different code languages and technologies related to the computing / informatic field. I can demonstrate an interesting flexibility as well as a will to learn new things, which led me to some knowledge as well as Back-End or Front-end side , design, UI, UX, SEO, motion design…  
With or without framework, on the technology you want, I totally adjust myself to your needs.   

I usually offer to my clients the most complete and versatile service as possible :  
  
💻 - Application and web development, plus their production  
📈 - Search Engine Optimization (SEO), for website referencing on plenty of browsers  
😎 - Design/Graphic Identity and models creation according to the request   
🔎 - A study of the user experience (UI/UX)  

I’m attentive to your needs and, thanks to my hard-working and responsible mind, I can suggest you the most fitting solutions. Moreover, my patience and my passion for my job will lead us to pleasing and lasting terms.   
You can rely on me for your project(s) and I remain at your disposal for all kind of opportunities !  
Don’t hesitate to get in touch with me for more information and answers to all your questioning.  
  
  
[![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=CinquinAndy&include_all_commits=true&show_icons=true&theme=midnight-purple)](https://github.com/anuraghazra/github-readme-stats)
